import request from '@/utils/request'

// 查询状态列表
export function listStatus(query) {
  return request({
    url: '/system/station/status',
    method: 'get',
    params: query
  })
}

// 淘汰公司
export function recStation(id) {
  return request({
    url: '/system/station/recover/' + id,
    method: 'delete'
  })
}
