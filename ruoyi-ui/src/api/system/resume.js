import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listResume(query) {
  return request({
    url: '/system/resume/list',
    method: 'get',
    params: query
  })
}

// 查询岗位
export function listStation(query) {
  return request({
    url: '/system/station/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getResume(id) {
  return request({
    url: '/system/resume/' + id,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addResume(data) {
  return request({
    url: '/system/resume',
    method: 'post',
    data: data
  })
}
// 批量推送简历
export function batchPush(rids,pid) {
  return request({
    url: '/system/resume/batchPush/'+rids+"/"+pid,
    method: 'post',

  })
}

// 推送简历
export function addPush(data) {
  return request({
    url: '/system/push',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateResume(data) {
  return request({
    url: '/system/resume',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delResume(id) {
  return request({
    url: '/system/resume/' + id,
    method: 'delete'
  })
}

export function uploadFile(data) {
  return request({
    url: 'system/resume/upload',
    method: 'post',
    data: data
  })
}

export function uploadFilePDF(data) {
  return request({
    url: 'system/resume/file',
    method: 'post',
    data: data
  })
}

export function pieResumeChartByYear() {
  return request({
    url: 'system/resume/pieChartByYear',
    method: 'get'
  })
}

export function lineResumeChartByYear() {
  return request({
    url: 'system/resume/lineChartByYear',
    method: 'get'
  })
}

export function pieResumeChartByMonth() {
  return request({
    url: 'system/resume/pieChartByMonth',
    method: 'get'
  })
}

export function lineResumeChartByMonth() {
  return request({
    url: 'system/resume/lineChartByMonth',
    method: 'get'
  })
}
