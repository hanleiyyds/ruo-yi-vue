import request from '@/utils/request'

// 查询列表
export function listPush(query) {
  return request({
    url: '/system/push/list',
    method: 'get',
    params: query
  })
}

export function listPush1(query) {
  return request({
    url: '/system/push/list1',
    method: 'get',
    params: query
  })
}

// 查询列表
export function listPushNull() {
  return request({
    url: '/system/push/list/1',
    method: 'get',
    params: ''
  })
}



// 查询详细
export function getPush(id) {
  return request({
    url: '/system/push/' + id,
    method: 'get'
  })
}

// 新增
export function addPush(data) {
  return request({
    url: '/system/push',
    method: 'post',
    data: data
  })
}

// 修改
export function updatePush(data) {
  return request({
    url: '/system/push',
    method: 'put',
    data: data
  })
}

// 删除
export function delPush(id) {
  return request({
    url: '/system/push/' + id,
    method: 'delete'
  })
}

// 修改状态
export function updateSide(id) {
  return request({
    url: '/system/push/updateSide/' + id,
    method: 'delete'
  })
}

// 修改状态
export function updateSide1(id) {
  return request({
    url: '/system/push/updateSide1/' + id,
    method: 'delete'
  })
}
