import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listStation(query) {
  return request({
    url: '/system/station/list',
    method: 'get',
    params: query
  })
}

// 查询详细
export function getStation(id) {
  return request({
    url: '/system/station/' + id,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addStation(data) {
  return request({
    url: '/system/station',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateStation(data) {
  return request({
    url: '/system/station',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delStation(id) {
  return request({
    url: '/system/station/' + id,
    method: 'delete'
  })
}

// 淘汰【请填写功能名称】
export function eliStation(id) {
  return request({
    url: '/system/station/eliminate/' + id,
    method: 'delete'
  })
}
//查询商务工作工作量
export function selectNamecount(){
  return request({
    url: '/system/station/sout',
    method: 'get',
    params: ''
  })
}

export function getStationResume(id) {
  return request({
    url: '/system/station/getStationResume/' + id,
    method: 'get'
  })
}

