import request from '@/utils/request'

// 淘汰【请填写功能名称】列表
export function taotaiStation(query) {
  return request({
    url: '/system/station/taotai',
    method: 'get',
    params: query
  })
}


// 恢复淘汰列表
export function recovery(id) {
  return request({
    url: '/system/station/recovery/' + id,
    method: 'delete',
  })
}

// 查询【请填写功能名称】列表
export function getStation(id) {
  return request({
    url: '/system/station/' + id,
    method: 'get',
  })
}



