import request from '@/utils/request'

export function exportPDF(params) {
  //以文件流的方式返回数据
  return request({
    url: '/exportPdf/getPdf',
    method: 'post',
    responseType:'blob',
    params: params
  })
}
