const name = "be756b96-c8b5-46c4-ab67-02e988973090.xlsx";
const isDelete = true;

// 默认下载方法
this.$download.name(name);

// 下载完成后是否删除文件
this.$download.name(name, isDelete);

const resource = "/profile/upload/2021/09/27/be756b96-c8b5-46c4-ab67-02e988973090.png";

// 默认方法
this.$download.resource(resource);

const url = "/tool/gen/batchGenCode?tables=" + tableNames;
const name = "ruoyi";

// 默认方法
this.$download.zip(url, name);

// 自定义文本保存
var blob = new Blob(["Hello, world!"], {type: "text/plain;charset=utf-8"});
this.$download.saveAs(blob, "hello world.txt");

// 自定义文件保存
var file = new File(["Hello, world!"], "hello world.txt", {type: "text/plain;charset=utf-8"});
this.$download.saveAs(file);

// 自定义data数据保存
const blob = new Blob([data], { type: 'text/plain;charset=utf-8' })
this.$download.saveAs(blob, name)

// 根据地址保存文件
this.$download.saveAs("https://ruoyi.vip/images/logo.png", "logo.jpg");

const baseURL = process.env.VUE_APP_BASE_API


//预览pdf
//(api,query);{...有问题
previewPdf(api,query)
{
  var url = baseURL + api + '?' + tansParams(query);  //tansParams() 是/utils/ruoyi.js中处理参数的方法
  axios({ //使用原生的axios请求原因后面有解释，请求要符合若依框架传参，参考request.js
    method: 'get',
    url: url,
    responseType: 'blob',
    params: {},
    headers: {'Authorization': 'Bearer ' + getToken()}
  }).then(res => {

    const isLogin = blobValidate(res.data);
    if (isLogin) {
      const blob = new Blob([res.data], {type: 'application/pdf'})
      let href = URL.createObjectURL(blob)
      if (process.env.NODE_ENV === 'development') {
        window.open('/pdfjs/web/viewer.html?file=' + encodeURIComponent(href));
      } else {
        window.open("/pdfjs部署在nginx中的路径/pdfjs/web/viewer.html?file=" + encodeURIComponent(href));
      }
    } else {
      this.printErrMsg(res.data);
    }
  })

}
//打印用印
this.$download.previewPdf("/approval/seal/previewPdf",{faultId:this.faultId})
