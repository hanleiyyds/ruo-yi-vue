package com.ruoyi.common.core.domain.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 ito_station
 *
 * @author ruoyi
 * @date 2022-10-27
 */
public class ItoStation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 招聘企业 */
    @Excel(name = "招聘企业")
    private String residentEnterprises;

    /** 岗位名称 */
    @Excel(name = "岗位名称")
    private String stationName;

    /** 学历要求 */
    @Excel(name = "学历要求")
    private String stationBackground;

    /** 工作经验 */
    @Excel(name = "工作经验")
    private String stationWork;

    /** 招聘人数 */
    @Excel(name = "招聘人数")
    private Long stationQuantity;

    /** 用工时长(月) */
    @Excel(name = "用工时长(月)")
    private Long workData;

    /** 最低薪资 */
    @Excel(name = "最低薪资")
    private String minimumWage;

    /** 最高薪资 */
    @Excel(name = "最高薪资")
    private String highestSalary;

    /** 面议 */
    @Excel(name = "面议")
    private String negotiable;

    /** 招聘截至时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "招聘截至时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date stationEndtimt;

    /** 查询开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "查询开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date beginTime;

    /** 查询结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "查询结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 工作地点 */
    @Excel(name = "工作地点")
    private String workPlace;

    /** 岗位职责 */
    @Excel(name = "岗位职责")
    private String jobResponsibilities;

    /** 岗位要求 */
    @Excel(name = "岗位要求")
    private String jobRequirements;

    /** 职位状态(0是正在招聘,1是招聘结束) */
    @Excel(name = "职位状态(0是正在招聘,1是招聘结束)")
    private Integer stationState;

    /** 删除状态(1是删除,0是未删除) */
    @Excel(name = "删除状态(1是删除,0是未删除)")
    private Integer isDelete;

    @Override
    public String toString() {
        return "ItoStation{" +
                "id=" + id +
                ", residentEnterprises='" + residentEnterprises + '\'' +
                ", stationName='" + stationName + '\'' +
                ", stationBackground='" + stationBackground + '\'' +
                ", stationWork='" + stationWork + '\'' +
                ", stationQuantity=" + stationQuantity +
                ", workData=" + workData +
                ", minimumWage='" + minimumWage + '\'' +
                ", highestSalary='" + highestSalary + '\'' +
                ", negotiable='" + negotiable + '\'' +
                ", stationEndtimt=" + stationEndtimt +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", workPlace='" + workPlace + '\'' +
                ", jobResponsibilities='" + jobResponsibilities + '\'' +
                ", jobRequirements='" + jobRequirements + '\'' +
                ", stationState=" + stationState +
                ", isDelete=" + isDelete +
                '}';
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResidentEnterprises() {
        return residentEnterprises;
    }

    public void setResidentEnterprises(String residentEnterprises) {
        this.residentEnterprises = residentEnterprises;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStationBackground() {
        return stationBackground;
    }

    public void setStationBackground(String stationBackground) {
        this.stationBackground = stationBackground;
    }

    public String getStationWork() {
        return stationWork;
    }

    public void setStationWork(String stationWork) {
        this.stationWork = stationWork;
    }

    public Long getStationQuantity() {
        return stationQuantity;
    }

    public void setStationQuantity(Long stationQuantity) {
        this.stationQuantity = stationQuantity;
    }

    public Long getWorkData() {
        return workData;
    }

    public void setWorkData(Long workData) {
        this.workData = workData;
    }

    public String getMinimumWage() {
        return minimumWage;
    }

    public void setMinimumWage(String minimumWage) {
        this.minimumWage = minimumWage;
    }

    public String getHighestSalary() {
        return highestSalary;
    }

    public void setHighestSalary(String highestSalary) {
        this.highestSalary = highestSalary;
    }

    public String getNegotiable() {
        return negotiable;
    }

    public void setNegotiable(String negotiable) {
        this.negotiable = negotiable;
    }

    public Date getStationEndtimt() {
        return stationEndtimt;
    }

    public void setStationEndtimt(Date stationEndtimt) {
        this.stationEndtimt = stationEndtimt;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getWorkPlace() {
        return workPlace;
    }

    public void setWorkPlace(String workPlace) {
        this.workPlace = workPlace;
    }

    public String getJobResponsibilities() {
        return jobResponsibilities;
    }

    public void setJobResponsibilities(String jobResponsibilities) {
        this.jobResponsibilities = jobResponsibilities;
    }

    public String getJobRequirements() {
        return jobRequirements;
    }

    public void setJobRequirements(String jobRequirements) {
        this.jobRequirements = jobRequirements;
    }

    public Integer getStationState() {
        return stationState;
    }

    public void setStationState(Integer stationState) {
        this.stationState = stationState;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}


