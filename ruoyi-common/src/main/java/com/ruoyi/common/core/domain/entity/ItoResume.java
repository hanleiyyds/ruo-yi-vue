package com.ruoyi.common.core.domain.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 ito_resume
 * 
 * @author ruoyi
 * @date 2022-10-12
 */
public class ItoResume extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 电话 */
    @Excel(name = "电话")
    private String phone;

    /** 参加工作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "参加工作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date workingTime;

    /** 当前求职状态 */
    @Excel(name = "当前求职状态")
    private String currentJobStatus;

    /** 职场人(我的牛人身份) */
    @Excel(name = "职场人(我的牛人身份)")
    private String workplacePeople;

    /** 个人优势 */
    @Excel(name = "个人优势")
    private String personalAdvantages;

    /** 求职类型 */
    @Excel(name = "求职类型")
    private String jobType;

    /** 期望职位 */
    @Excel(name = "期望职位")
    private String expectedPosition;

    /** 薪资要求 */
    @Excel(name = "薪资要求")
    private String salaryRequirements;

    /** 工作城市 */
    @Excel(name = "工作城市")
    private String workCity;

    /** 期望行业 */
    @Excel(name = "期望行业")
    private String expectedIndustry;

    /** 公司名称 */
    @Excel(name = "公司名称")
    private String corporateName;

    /** 所属部门 */
    @Excel(name = "所属部门")
    private String department;

    /** 在职时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "在职时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date serviceTime;

    /** 所属行业 */
    @Excel(name = "所属行业")
    private String industry;

    /** 职位名称 */
    @Excel(name = "职位名称")
    private String positionName;

    /** 工作内容 */
    @Excel(name = "工作内容")
    private String jobContent;

    /** 拥有技能 */
    @Excel(name = "拥有技能")
    private String haveSkills;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String entryName;

    /** 项目角色 */
    @Excel(name = "项目角色")
    private String projectRole;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 项目描述 */
    @Excel(name = "项目描述")
    private String projectDescription;

    /** 学校名称 */
    @Excel(name = "学校名称")
    private String schoolName;

    /** 学制类型 */
    @Excel(name = "学制类型")
    private String typeSchooling;

    /** 学历 */
    @Excel(name = "学历")
    private String education;

    /** 专业 */
    @Excel(name = "专业")
    private String major;

    /** 创建人ID */
    @Excel(name = "创建人ID")
    private Long creator;

    /** 逻辑删除 */
    @Excel(name = "逻辑删除")
    private Integer isDelete;

    /** 淘汰状态,正常状态,(采购淘汰过期简历) */
    @Excel(name = "淘汰状态,正常状态,(采购淘汰过期简历)")
    private Integer state;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(Date workingTime) {
        this.workingTime = workingTime;
    }

    public String getCurrentJobStatus() {
        return currentJobStatus;
    }

    public void setCurrentJobStatus(String currentJobStatus) {
        this.currentJobStatus = currentJobStatus;
    }

    public String getWorkplacePeople() {
        return workplacePeople;
    }

    public void setWorkplacePeople(String workplacePeople) {
        this.workplacePeople = workplacePeople;
    }

    public String getPersonalAdvantages() {
        return personalAdvantages;
    }

    public void setPersonalAdvantages(String personalAdvantages) {
        this.personalAdvantages = personalAdvantages;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getExpectedPosition() {
        return expectedPosition;
    }

    public void setExpectedPosition(String expectedPosition) {
        this.expectedPosition = expectedPosition;
    }

    public String getSalaryRequirements() {
        return salaryRequirements;
    }

    public void setSalaryRequirements(String salaryRequirements) {
        this.salaryRequirements = salaryRequirements;
    }

    public String getWorkCity() {
        return workCity;
    }

    public void setWorkCity(String workCity) {
        this.workCity = workCity;
    }

    public String getExpectedIndustry() {
        return expectedIndustry;
    }

    public void setExpectedIndustry(String expectedIndustry) {
        this.expectedIndustry = expectedIndustry;
    }

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Date getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(Date serviceTime) {
        this.serviceTime = serviceTime;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getJobContent() {
        return jobContent;
    }

    public void setJobContent(String jobContent) {
        this.jobContent = jobContent;
    }

    public String getHaveSkills() {
        return haveSkills;
    }

    public void setHaveSkills(String haveSkills) {
        this.haveSkills = haveSkills;
    }

    public String getEntryName() {
        return entryName;
    }

    public void setEntryName(String entryName) {
        this.entryName = entryName;
    }

    public String getProjectRole() {
        return projectRole;
    }

    public void setProjectRole(String projectRole) {
        this.projectRole = projectRole;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getTypeSchooling() {
        return typeSchooling;
    }

    public void setTypeSchooling(String typeSchooling) {
        this.typeSchooling = typeSchooling;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "ItoResume{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", sex='" + sex + '\'' +
                ", phone='" + phone + '\'' +
                ", workingTime=" + workingTime +
                ", currentJobStatus='" + currentJobStatus + '\'' +
                ", workplacePeople='" + workplacePeople + '\'' +
                ", personalAdvantages='" + personalAdvantages + '\'' +
                ", jobType='" + jobType + '\'' +
                ", expectedPosition='" + expectedPosition + '\'' +
                ", salaryRequirements='" + salaryRequirements + '\'' +
                ", workCity='" + workCity + '\'' +
                ", expectedIndustry='" + expectedIndustry + '\'' +
                ", corporateName='" + corporateName + '\'' +
                ", department='" + department + '\'' +
                ", serviceTime=" + serviceTime +
                ", industry='" + industry + '\'' +
                ", positionName='" + positionName + '\'' +
                ", jobContent='" + jobContent + '\'' +
                ", haveSkills='" + haveSkills + '\'' +
                ", entryName='" + entryName + '\'' +
                ", projectRole='" + projectRole + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", projectDescription='" + projectDescription + '\'' +
                ", schoolName='" + schoolName + '\'' +
                ", typeSchooling='" + typeSchooling + '\'' +
                ", education='" + education + '\'' +
                ", major='" + major + '\'' +
                ", creator=" + creator +
                ", isDelete=" + isDelete +
                ", state=" + state +
                '}';
    }
}
