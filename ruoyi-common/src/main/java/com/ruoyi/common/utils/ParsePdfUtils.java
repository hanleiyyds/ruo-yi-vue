package com.ruoyi.common.utils;

import com.ruoyi.common.core.domain.entity.ItoResume;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class ParsePdfUtils {
    public final String PERSONAL_STRENGTHS = "个人优势";
    public final String WORK_EXPERIENCE = "工作经历";
    public final String PROJECT_EXPERIENCE = "项目经历";
    public final String EDUCATION_EXPERIENCE = "教育经历";

    public void parse(String path,ItoResume itoResume) throws IOException {
        File file = new File(path);
        //加载现有PDF文档
        PDDocument load = PDDocument.load(file);
        PDFTextStripper stripper = new PDFTextStripper();
        stripper.setSortByPosition(true);
        String text = stripper.getText(load);
        String[] split = text.split("\r\n");
        //基本信息
        basicInformation(split,itoResume);
        //个人优势
        String personalAdvantages = fetchPersonalStrengths(split);
        itoResume.setPersonalAdvantages(personalAdvantages);
        //工作经历
        fetchWorkExperience(split,itoResume);
        //项目经历
        fetchProjectExperience(split,itoResume);
        //教育经历
        fetchEducationExperience(split,itoResume);
        load.close();
    }

    //取得基本信息
    public void basicInformation(String[] text,ItoResume itoResume){
        //获取名字
        itoResume.setName(text[0]);
        String[] split = text[1].split(" | ");
        //获取手机号
        itoResume.setPhone(split[0]);
        //性别
        itoResume.setSex(split[2]);
        //生日
        String s = split[4].split("：")[1];
        //2003.07
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM");
        try {
            Date parse = simpleDateFormat.parse(s);
            itoResume.setBirthday(parse);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String[] split1 = text[2].split(" | ");
        //工作城市
        itoResume.setWorkCity(split1[0]);
        //工作时间
        String workingTime = split1[2].split("：")[1];
        //获取当前时间
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        //判断是年还是月，减去工作时间
        if (workingTime.contains("年")){
            int time = Integer.parseInt(workingTime.substring(0, 1));
            calendar.setTime(date);
            calendar.add(Calendar.YEAR, -time);
        }else if (workingTime.contains("月")){
            int time = Integer.parseInt(workingTime.substring(0, 1));
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, -time);
        }
        itoResume.setWorkingTime(calendar.getTime());
        //期望职位
        itoResume.setExpectedPosition(split1[4].split("：")[1]);
        //期望薪资
        itoResume.setSalaryRequirements(split1[6].split("：")[1]);
    }

    //获取个人优势
    public String fetchPersonalStrengths(String[] text){
        int startSubscripting = 0;
        int endSubscripting = 0;
        for (int i = 0; i < text.length; i++) {
            if (PERSONAL_STRENGTHS.equals(text[i])){
                startSubscripting = i;
            }else if (WORK_EXPERIENCE.equals(text[i])){
                endSubscripting = i;
            }
        }
        return Arrays.toString(Arrays.copyOfRange(text, startSubscripting + 1, endSubscripting)).replaceAll("]","").replaceAll("\\[","");
    }

    //获取工作经历
    public void fetchWorkExperience(String[] text,ItoResume itoResume){
        int startSubscripting = 0;
        int endSubscripting = 0;
        for (int i = 0; i < text.length; i++) {
            if (WORK_EXPERIENCE.equals(text[i])){
                startSubscripting = i;
            }else if (PROJECT_EXPERIENCE.equals(text[i])){
                endSubscripting = i;
            }
        }
        String[] strings = Arrays.copyOfRange(text, startSubscripting + 1, endSubscripting);
        String workExperience = Arrays.toString(strings).replaceAll("]", "").replaceAll("\\[", "");
        itoResume.setJobContent(workExperience);
    }

    //获取项目经历
    public void fetchProjectExperience(String[] text,ItoResume itoResume){
        int startSubscripting = 0;
        int endSubscripting = 0;
        for (int i = 0; i < text.length; i++) {
            if (PROJECT_EXPERIENCE.equals(text[i])){
                startSubscripting = i;
            }else if (EDUCATION_EXPERIENCE.equals(text[i])){
                endSubscripting = i;
            }
        }
        String[] strings = Arrays.copyOfRange(text, startSubscripting + 1, endSubscripting);
        String projectException = Arrays.toString(strings).replaceAll("]", "").replaceAll("\\[", "");
        itoResume.setProjectDescription(projectException);
    }

    //获取教育经历
    public void fetchEducationExperience(String[] text,ItoResume itoResume){
        int startSubscripting = 0;
        for (int i = 0; i < text.length; i++) {
            if (EDUCATION_EXPERIENCE.equals(text[i])){
                startSubscripting = i;
            }
        }
        String[] strings = Arrays.copyOfRange(text, startSubscripting + 1, text.length);
        String[] s = strings[0].split(" ");
        itoResume.setSchoolName(s[0]);
        //学历
        itoResume.setEducation(s[2]);
        //专业
        itoResume.setMajor(s[4]);
        //学制类型
        String[] split = s[5].split("-");
        itoResume.setTypeSchooling(Integer.valueOf(split[1]) - Integer.valueOf(split[0])+"");
    }
}
