package com.ruoyi.system.domain.vo;

import java.util.List;

public class LineResponseVo {

    private List<String> name;
    private List<String> data;

    public LineResponseVo() {
    }

    public LineResponseVo(List<String> name, List<String> data) {
        this.name = name;
        this.data = data;
    }

    public List<String> getName() {
        return name;
    }

    public void setName(List<String> name) {
        this.name = name;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "LineResponseVo{" +
                "name=" + name +
                ", data=" + data +
                '}';
    }
}
