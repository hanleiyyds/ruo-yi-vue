package com.ruoyi.system.domain.po;

public class ChartsInquireByMonthPo {

    private String name;
    private String value;

    public ChartsInquireByMonthPo() {
    }

    public ChartsInquireByMonthPo(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ChartsInquireByMonthPo{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
