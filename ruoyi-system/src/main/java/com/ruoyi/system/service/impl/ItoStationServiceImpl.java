package com.ruoyi.system.service.impl;


import com.ruoyi.common.core.domain.entity.ItoPush;
import com.ruoyi.common.core.domain.entity.ItoResume;
import com.ruoyi.common.core.domain.entity.ItoStation;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.mapper.ItoPushMapper;
import com.ruoyi.system.mapper.ItoResumeMapper;
import com.ruoyi.system.mapper.ItoStationMapper;
import com.ruoyi.system.service.IItoStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2022-10-12
 */
@Service
public class ItoStationServiceImpl implements IItoStationService {
    @Autowired
    private ItoStationMapper itoStationMapper;
    @Autowired
    private ItoResumeMapper itoResumeMapper;
    @Autowired
    private ItoPushMapper itoPushMapper;

    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public ItoStation selectItoStationById(Long id) {
        return itoStationMapper.selectItoStationById(id);
    }

//    @Override
//    public List<ItoResume> getStationResume(Long id) {
//
//        ItoStation itoStation = itoStationMapper.selectItoStationById(id);
//        System.out.println("itoStation:" + itoStation);
//
//        ItoPush itoPush = new ItoPush();
//        itoPush.setPid(itoStation.getId());
//        List<ItoPush> itoPushes = itoPushMapper.selectItoPushList();
//        System.out.println("itoPushes:" + itoPushes);
//
//        ItoResume itoResume = new ItoResume();
//        itoResume.setId(itoPushes.get(1).getRid());
//        List<ItoResume> itoResumes = itoResumeMapper.selectItoResumeList(itoResume);
//        System.out.println("itoResumes:" + itoResumes);
//
//        return null;
//    }

    /**
     * 查询【请填写功能名称】列表
     *
     * @param itoStation 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<ItoStation> selectItoStationList(ItoStation itoStation) {
        itoStation.setIsDelete(0);
        itoStation.setStationState(0);
        return itoStationMapper.selectItoStationList(itoStation);
    }

    /**
     * 历史岗位列表
     *
     * @param itoStation 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<ItoStation> selectItoStationsList(ItoStation itoStation) {
//        itoStation.setIsDelete(0);
        itoStation.setStationState(1);
        return itoStationMapper.selectItoStationList(itoStation);
    }

    /**
     * 淘汰岗位恢复
     *
     * @param 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public int recovery(Long[] ids) {
        int a = 0;
        for (int i = 0; i < ids.length; i++) {
            long id = ids[i];
            itoStationMapper.selectItoStationById(id);
            ItoStation itoStation = itoStationMapper.selectItoStationById(id);
            itoStation.setStationState(0);
            a = itoStationMapper.updateItoStation(itoStation);
        }
        return a;
    }


    /**
     * 新增【请填写功能名称】
     *
     * @param itoStation 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertItoStation(ItoStation itoStation) {
//        itoStation.setStationRelease(SecurityUtils.getUsername());
        itoStation.setCreateTime(DateUtils.getNowDate());
        itoStation.setUpdateTime(DateUtils.getNowDate());
        itoStation.setStationState(0);
        itoStation.setIsDelete(0);
        itoStation.setCreateTime(DateUtils.getNowDate());
        return itoStationMapper.insertItoStation(itoStation);
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param itoStation 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateItoStation(ItoStation itoStation) {
//        itoStation.setStationRelease(SecurityUtils.getUsername());
        itoStation.setUpdateTime(DateUtils.getNowDate());
        return itoStationMapper.updateItoStation(itoStation);
    }

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteItoStationByIds(Long[] ids) {
        int a = 0;
        for (int i = 0; i < ids.length; i++) {
            long id = ids[i];
            itoStationMapper.selectItoStationById(id);
            ItoStation itoStation = itoStationMapper.selectItoStationById(id);
            itoStation.setIsDelete(1);
            a = itoStationMapper.updateItoStation(itoStation);
        }
        return a;
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteItoStationById(Long id) {
        return itoStationMapper.deleteItoStationById(id);
    }

    /**
     * 淘汰【请填写功能名称】信息
     *
     * @param 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int eliStation(Long[] ids) {
        int a = 0;
        for (int i = 0; i < ids.length; i++) {
            long id = ids[i];
            ItoStation itoStation = itoStationMapper.selectItoStationById(id);
            itoStation.setStationState(1);
            a = itoStationMapper.updateItoStation(itoStation);
        }
        return a;
    }

    /**
     * 统计【商务数量】信息
     *
     * @param 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int selectname(String zname) {
        return itoStationMapper.selectNamecount(zname);
    }
}
