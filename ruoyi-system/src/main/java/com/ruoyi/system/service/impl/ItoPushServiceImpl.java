package com.ruoyi.system.service.impl;


import com.ruoyi.common.core.domain.entity.*;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.mapper.ItoPushMapper;
import com.ruoyi.system.mapper.ItoResumeMapper;
import com.ruoyi.system.service.IItoPushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-12
 */
@Service
public class ItoPushServiceImpl implements IItoPushService
{
    @Autowired
    private ItoPushMapper itoPushMapper;

    @Autowired
    ItoResumeMapper itoResumeMapper;

    /**
     * 查询
     * 
     * @param id 主键
     * @return
     */
    @Override
    public ItoPush selectItoPushById(Long id)
    {

        return itoPushMapper.selectItoPushById(id);
    }



    /**
     * 查询【请填写功能名称】列表
     * 
     * @param itoPush 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<ItoPushVo> selectItoPushList(ItoPushVo itoPush) {
        itoPush.setSide(1);
        itoPush.setSide(2);
        List<ItoPushVo> itoPushVos = itoPushMapper.selectItoPushList(itoPush);
        return itoPushVos;
    }
    /**
     * 新增【请填写功能名称】
     * 
     * @param itoPush 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertItoPush(ItoPush itoPush) {
        itoPush.setSide(0);
        itoPush.setCreateTime(DateUtils.getNowDate());
        SysUser user = SecurityUtils.getLoginUser().getUser();
        itoPush.setCreateTime(DateUtils.getNowDate());
        itoPush.setUpdateTime(DateUtils.getNowDate());
        itoPush.setUid(user.getUserId());
        int i = itoPushMapper.insertItoPush(itoPush);
        if (i != 0) {
            ItoResume itoResume = new ItoResume();
            itoResume.setId(itoPush.getRid());
            itoResume.setState(1);
            itoResumeMapper.updateItoResume(itoResume);
        }
        return i;
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param itoPush 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateItoPush(ItoPush itoPush) {
        itoPush.setSide(1);
        itoPush.setUpdateTime(DateUtils.getNowDate());
        return itoPushMapper.updateItoPush(itoPush);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteItoPushByIds(Long[] ids) {


        for (int i = 0; i < ids.length; i++) {
            Long id = ids[i];
            ItoPush itoPush = itoPushMapper.selectItoPushById(id);
            return itoPushMapper.updateItoPush(itoPush);
        }
        return 1;
    }



    @Override
    public int updateItoPushByIds(Long[] ids) {
        for (int i = 0; i < ids.length; i++) {
            Long id = ids[i];
            ItoPush itoPush = itoPushMapper.selectItoPushById(id);
            itoPush.setSide(1);
            return itoPushMapper.updateItoPush(itoPush);
        }
        return 1;
    }

    @Override
    public int updateItoPushByIds1(Long[] ids) {
        for (int i = 0; i < ids.length; i++) {
            Long id = ids[i];
            ItoPush itoPush = itoPushMapper.selectItoPushById(id);
            itoPush.setSide(2);
            return itoPushMapper.updateItoPush(itoPush);
        }
        return 1;
    }
}
