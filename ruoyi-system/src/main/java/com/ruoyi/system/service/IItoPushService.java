package com.ruoyi.system.service;


import com.ruoyi.common.core.domain.entity.ItoListPush;
import com.ruoyi.common.core.domain.entity.ItoPush;
import com.ruoyi.common.core.domain.entity.ItoPushVo;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2022-10-12
 */
public interface IItoPushService {
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ItoPush selectItoPushById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param itoPush 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ItoPushVo> selectItoPushList(ItoPushVo itoPush);



    /**
     * 新增【请填写功能名称】
     * 
     * @param itoPush 【请填写功能名称】
     * @return 结果
     */
    public int insertItoPush(ItoPush itoPush);

    /**
     * 修改【请填写功能名称】
     * 
     * @param itoPush 【请填写功能名称】
     * @return 结果
     */
    public int updateItoPush(ItoPush itoPush);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
     int deleteItoPushByIds(Long[] ids);

    int updateItoPushByIds(Long[] ids);

    int updateItoPushByIds1(Long[] ids);
}
