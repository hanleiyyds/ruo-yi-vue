package com.ruoyi.system.service;


import com.ruoyi.common.core.domain.entity.ItoPush;
import com.ruoyi.common.core.domain.entity.ItoResume;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import com.ruoyi.system.domain.vo.LineResponseVo;
import com.ruoyi.system.domain.vo.PieResponseVo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2022-10-12
 */
public interface IItoResumeService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ItoResume selectItoResumeById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param itoResume 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ItoResume> selectItoResumeList(ItoResume itoResume);

    /**
     * 新增【请填写功能名称】
     * 
     * @param itoResume 【请填写功能名称】
     * @return 结果
     */
    public int insertItoResume(ItoResume itoResume);

    /**
     * 修改【请填写功能名称】
     * 
     * @param itoResume 【请填写功能名称】
     * @return 结果
     */
    public int updateItoResume(ItoResume itoResume);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteItoResumeByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id【请填写功能名称】主键
     * @return 结果
     */
    public int deleteItoResumeById(Long id);

    int eliResume(Long[] ids);

    int PushResume(ItoPush itoPush);

    int pushResumes(Long[] rids, Long pid);




    /**
     * 生成PDF
     * @param
     * @param response 响应
     */
 //   void generatorAdmissionCard(AdmissionCard admissionCard, HttpServletResponse response) throws UnsupportedEncodingException, FileNotFoundException;

    /**
     * 上传简历文件 pdf格式
     * @param file
     */
    ItoResume uploadFilePdf(MultipartFile file, String filePath) throws InvalidExtensionException;

    //饼图查询
    List<PieResponseVo> pieChartSelectByYear();

    //线图查询按年
    LineResponseVo LineChartSelectByYear();

    //饼图查询按月
    List<PieResponseVo> pieChartSelectByMonth();

    //饼图查询按年
    LineResponseVo LineChartSelectByMonth();
}
