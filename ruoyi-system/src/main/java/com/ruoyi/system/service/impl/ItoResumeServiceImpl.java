package com.ruoyi.system.service.impl;


import com.ruoyi.common.core.domain.entity.ItoPush;
import com.ruoyi.common.core.domain.entity.ItoResume;
import com.ruoyi.common.core.domain.entity.ItoStation;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.exception.file.FileNameLengthLimitExceededException;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ParsePdfUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.MimeTypeUtils;
import com.ruoyi.system.domain.po.ChartsInquireByMonthPo;
import com.ruoyi.system.domain.po.ChartsInquireByYearPo;
import com.ruoyi.system.domain.vo.LineResponseVo;
import com.ruoyi.system.domain.vo.PieResponseVo;
import com.ruoyi.system.mapper.ItoPushMapper;
import com.ruoyi.system.mapper.ItoResumeMapper;
import com.ruoyi.system.mapper.ItoStationMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.IItoResumeService;
import com.ruoyi.system.service.ISysConfigService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.web.multipart.MultipartFile;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2022-10-12
 */
@Service

public class ItoResumeServiceImpl implements IItoResumeService {

    private static final org.slf4j.Logger log = (org.slf4j.Logger) LoggerFactory.getLogger(ItoResumeServiceImpl.class);
    @Autowired
    private ItoResumeMapper itoResumeMapper;

    @Autowired
    private ItoStationMapper itoStationMapper;


    @Autowired
    private ItoPushMapper itoPushMapper;


    @Autowired
    private ISysConfigService configService;
    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public ItoResume selectItoResumeById(Long id) {


        return itoResumeMapper.selectItoResumeById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     *
     * @param itoResume 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<ItoResume> selectItoResumeList(ItoResume itoResume) {
        itoResume.setIsDelete(0);
        itoResume.setState(0);
        return itoResumeMapper.selectItoResumeList(itoResume);
    }

    /**
     * 新增【请填写功能名称】
     *
     * @param itoResume 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertItoResume(ItoResume itoResume) {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        itoResume.setCreator(user.getUserId());
        itoResume.setCreateTime(DateUtils.getNowDate());
        itoResume.setUpdateTime(DateUtils.getNowDate());
        itoResume.setIsDelete(0);
        return itoResumeMapper.insertItoResume(itoResume);
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param itoResume 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateItoResume(ItoResume itoResume) {
        itoResume.setUpdateTime(DateUtils.getNowDate());
        return itoResumeMapper.updateItoResume(itoResume);
    }

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteItoResumeByIds(Long[] ids) {
        int t = 0;
        for (int i = 0; i < ids.length; i++) {
            long id = ids[i];
            ItoResume resume = itoResumeMapper.selectItoResumeById(id);
            resume.setIsDelete(1);
            t = itoResumeMapper.updateItoResume(resume);
        }
        return t;
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteItoResumeById(Long id) {

        ItoResume resume = itoResumeMapper.selectItoResumeById(id);
        resume.setIsDelete(1);
        return itoResumeMapper.updateItoResume(resume);
    }

    @Override
    public int eliResume(Long[] ids) {


        for (int i = 0; i < ids.length; i++) {
            long id = ids[i];
            ItoResume resume = itoResumeMapper.selectItoResumeById(id);
            resume.setState(1);
            return itoResumeMapper.updateItoResume(resume);
        }


        return 0;
    }

    @Override
    public int PushResume(ItoPush itoPush) {

        ItoStation itoStation = new ItoStation();
        List<ItoStation> itoStations = itoStationMapper.selectItoStationList(itoStation);
        itoPush.setPid(itoStations.get(0).getId());

        ItoResume itoResume = new ItoResume();
        List<ItoResume> itoResumes = itoResumeMapper.selectItoResumeList(itoResume);
        itoPush.setRid(itoResumes.get(0).getId());


        SysUser user = SecurityUtils.getLoginUser().getUser();
        itoPush.setCreateTime(DateUtils.getNowDate());
        itoPush.setUpdateTime(DateUtils.getNowDate());
        itoPush.setUid(user.getUserId());
        int t = itoPushMapper.insertItoPush(itoPush);
        System.out.println(t);
        if (t > 0) {
            itoResume.setState(1);
            itoResumeMapper.updateItoResume(itoResume);
        }
        return t;
    }

    @Override
    public int pushResumes(Long[] rids, Long pid) {
        ItoResume itoResume = new ItoResume();
        ItoPush itoPush = new ItoPush();
        SysUser user = SecurityUtils.getLoginUser().getUser();
        itoResume.setCreator(user.getUserId());
        for (Long rid : rids) {
            itoPush.setRid(rid);
            itoPush.setPid(pid);
            itoPush.setUid(user.getUserId());
            itoPush.setCreateTime(DateUtils.getNowDate());
            itoPush.setUpdateTime(DateUtils.getNowDate());
            itoPushMapper.insertItoPush(itoPush);
            itoResume.setState(1);
            itoResume.setId(rid);
            System.out.println(rid);
            itoResume.setCreator(user.getUserId());
            itoResumeMapper.updateItoResume(itoResume);
        }

        return 1;
    }


    @Override
    public ItoResume uploadFilePdf(MultipartFile file,String filePath) throws InvalidExtensionException {
        if (file.isEmpty()) {
            throw new GlobalException("文件未上传");
        }

        File file_dir = new File(filePath);
        if (!file_dir.exists()) {
            file_dir.mkdirs();
        }
        String filename = file.getOriginalFilename(); //获取上传文件原来的名称
        String lastName = filename.substring(filename.lastIndexOf("."));
        String newName = UUID.randomUUID().toString() + lastName;

        int fileNamelength = file.getOriginalFilename().length();
        if (fileNamelength > FileUploadUtils.DEFAULT_FILE_NAME_LENGTH) {
            throw new FileNameLengthLimitExceededException(FileUploadUtils.DEFAULT_FILE_NAME_LENGTH);
        }
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.DEFAULT_ALLOWED_EXTENSION);
        File desc = new File(filePath + "/" + newName);
        // File desc1 = createAbsoluteFile(filePath + newName);
        ItoResume itoResume = new ItoResume();
        ParsePdfUtils parsePdfUtils = new ParsePdfUtils();
        try {
            file.transferTo(desc); //把上传的文件保存至本地
            parsePdfUtils.parse(desc.getPath(), itoResume);
            System.out.println(itoResume);
//            itoResume.setImgPath(newName);
        } catch (IOException e) {
            throw new GlobalException("上传失败");
        }
        return itoResume;
    }

    //饼图查询按年
    @Override
    public List<PieResponseVo> pieChartSelectByYear() {
        List<ChartsInquireByYearPo> chartsInquireByYearPos = itoResumeMapper.chartsSelectByYear();
        List<PieResponseVo> pieResponseVos = chartsInquireByYearPos.stream().map(chartsInquireByYearPo -> {
            PieResponseVo pieResponseVo = new PieResponseVo();
            BeanUtils.copyProperties(chartsInquireByYearPo, pieResponseVo);
            return pieResponseVo;
        }).collect(Collectors.toList());
        return pieResponseVos;
    }

    //线图查询按年
    @Override
    public LineResponseVo LineChartSelectByYear() {
        List<ChartsInquireByYearPo> chartsInquireByYearPos = itoResumeMapper.chartsSelectByYear();
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> data = new ArrayList<>();
        chartsInquireByYearPos.forEach(chartsInquireByYearPo -> {
            name.add(chartsInquireByYearPo.getName());
            data.add(chartsInquireByYearPo.getValue());
        });
        LineResponseVo lineResponseVo = new LineResponseVo();
        lineResponseVo.setName(name);
        lineResponseVo.setData(data);
        return lineResponseVo;
    }

    //饼图查询按年
    @Override
    public List<PieResponseVo> pieChartSelectByMonth() {
        List<ChartsInquireByMonthPo> chartsInquireByMonthPos = itoResumeMapper.chartsSelectByMonth();
        List<PieResponseVo> pieResponseVos = chartsInquireByMonthPos.stream().map(chartsInquireByMonthPo -> {
            PieResponseVo pieResponseVo = new PieResponseVo();
            BeanUtils.copyProperties(chartsInquireByMonthPo, pieResponseVo);
            return pieResponseVo;
        }).collect(Collectors.toList());
        return pieResponseVos;
    }

    //饼图查询按月
    @Override
    public LineResponseVo LineChartSelectByMonth() {
        List<ChartsInquireByMonthPo> chartsInquireByMonthPos = itoResumeMapper.chartsSelectByMonth();
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> data = new ArrayList<>();
        chartsInquireByMonthPos.forEach(chartsInquireByYearPo -> {
            name.add(chartsInquireByYearPo.getName());
            data.add(chartsInquireByYearPo.getValue());
        });
        LineResponseVo lineResponseVo = new LineResponseVo();
        lineResponseVo.setName(name);
        lineResponseVo.setData(data);
        return lineResponseVo;
    }
}
