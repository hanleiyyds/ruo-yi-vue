package com.ruoyi.system.service;


import com.ruoyi.common.core.domain.entity.ItoResume;
import com.ruoyi.common.core.domain.entity.ItoStation;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2022-10-12
 */
public interface IItoStationService {
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ItoStation selectItoStationById(Long id);

//    public List<ItoResume> getStationResume(Long id);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param itoStation 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ItoStation> selectItoStationList(ItoStation itoStation);

    /**
     * 新增【请填写功能名称】
     *
     * @param itoStation 【请填写功能名称】
     * @return 结果
     */
    public int insertItoStation(ItoStation itoStation);

    /**
     * 修改【请填写功能名称】
     *
     * @param itoStation 【请填写功能名称】
     * @return 结果
     */
    public int updateItoStation(ItoStation itoStation);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteItoStationByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteItoStationById(Long id);

    int eliStation(Long[] ids);

    List<ItoStation> selectItoStationsList(ItoStation itoStation);


    int recovery(Long[] ids);

    public int selectname(String zname);


}
