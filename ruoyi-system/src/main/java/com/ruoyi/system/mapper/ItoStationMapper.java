package com.ruoyi.system.mapper;


import com.ruoyi.common.core.domain.entity.ItoStation;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2022-10-12
 */
public interface ItoStationMapper {
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ItoStation selectItoStationById(Long id);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param itoStation 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ItoStation> selectItoStationList(ItoStation itoStation);

    /**
     * 新增【请填写功能名称】
     *
     * @param itoStation 【请填写功能名称】
     * @return 结果
     */
    public int insertItoStation(ItoStation itoStation);

    /**
     * 修改【请填写功能名称】
     *
     * @param itoStation 【请填写功能名称】
     * @return 结果
     */
    public int updateItoStation(ItoStation itoStation);

    /**
     * 删除【请填写功能名称】
     *
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteItoStationById(Long id);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteItoStationByIds(Long[] ids);

    public int selectNamecount(String zname);
}
