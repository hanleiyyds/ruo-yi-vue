package com.ruoyi.system.mapper;


import com.ruoyi.common.core.domain.entity.ItoResume;
import com.ruoyi.system.domain.po.ChartsInquireByMonthPo;
import com.ruoyi.system.domain.po.ChartsInquireByYearPo;
import com.ruoyi.system.domain.vo.PieResponseVo;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-12
 */
public interface ItoResumeMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ItoResume selectItoResumeById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param itoResume 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ItoResume> selectItoResumeList(ItoResume itoResume);

    /**
     * 新增【请填写功能名称】
     * 
     * @param itoResume 【请填写功能名称】
     * @return 结果
     */
    public int insertItoResume(ItoResume itoResume);

    /**
     * 修改【请填写功能名称】
     * 
     * @param itoResume 【请填写功能名称】
     * @return 结果
     */
    public int updateItoResume(ItoResume itoResume);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteItoResumeById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteItoResumeByIds(Long[] ids);


    ItoResume selectItoResumeByName(String name);

    //统计图查询按年
    List<ChartsInquireByYearPo> chartsSelectByYear();

    List<ChartsInquireByMonthPo> chartsSelectByMonth();
}
