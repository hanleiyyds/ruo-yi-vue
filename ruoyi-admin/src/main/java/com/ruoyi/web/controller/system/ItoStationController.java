package com.ruoyi.web.controller.system;


import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.ItoStation;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.service.IItoStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 【请填写功能名称】Controller
 *
 * @author ruoyi
 * @date 2022-10-12
 */
@RestController
@RequestMapping("/system/station")
public class ItoStationController extends BaseController {
    @Autowired
    private IItoStationService itoStationService;


//    /**
//     * 查询【请填写功能名称】列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:station:push')")
//    @GetMapping("/push")
//    public TableDataInfo push(ItoPush itoPush) {
//        startPage();
//        List<ItoPush> list = itoStationService.selectPushList(itoPush);
//        return getDataTable(list);
//    }



    /**
     * 查询【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('system:station:list')")
    @GetMapping("/list")
    public TableDataInfo list(ItoStation itoStation) {
        startPage();
        List<ItoStation> list = itoStationService.selectItoStationList(itoStation);
        return getDataTable(list);
    }

    /**
     * 岗位淘汰列表
     */
    @PreAuthorize("@ss.hasPermi('system:station:taotai')")
    @GetMapping("/taotai")
    public TableDataInfo taotai(ItoStation itoStation) {
        startPage();
        List<ItoStation> list = itoStationService.selectItoStationsList(itoStation);
        return getDataTable(list);
    }


    /**
     * 岗位淘汰恢复
     */
    @PreAuthorize("@ss.hasPermi('system:station:recovery')")
    @Log(title = "【恢复】", businessType = BusinessType.DELETE)
    @DeleteMapping("/recovery/{ids}")
    public AjaxResult recovery(@PathVariable Long[] ids) {
        return toAjax(itoStationService.recovery(ids));
    }


    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('system:station:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ItoStation itoStation) {
        List<ItoStation> list = itoStationService.selectItoStationList(itoStation);
        ExcelUtil<ItoStation> util = new ExcelUtil<ItoStation>(ItoStation.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:station:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(itoStationService.selectItoStationById(id));
    }

//
//    @PreAuthorize("@ss.hasPermi('system:station:getStationResume')")
//    @GetMapping(value = "/getStationResume/{id}")
//    public AjaxResult getStationResume(@PathVariable("id") Long id) {
//        return AjaxResult.success(itoStationService.getStationResume(id));
//    }


    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:station:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ItoStation itoStation) {
        return toAjax(itoStationService.insertItoStation(itoStation));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:station:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ItoStation itoStation) {
        return toAjax(itoStationService.updateItoStation(itoStation));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:station:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(itoStationService.deleteItoStationByIds(ids));
    }

    /**
     * 淘汰【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:station:eliminate')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("eliminate/{ids}")
    public AjaxResult eliminate(@PathVariable Long[] ids) {
        return toAjax(itoStationService.eliStation(ids));
    }


    /**
     * 统计【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:station:sout')")
    @GetMapping("/sout")
    public Integer stat() {
        String username = SecurityUtils.getUsername();
        int affairsCout = itoStationService.selectname(username);
        return affairsCout;
    }

}
