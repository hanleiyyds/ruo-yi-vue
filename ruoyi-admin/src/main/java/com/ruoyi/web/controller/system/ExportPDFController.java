package com.ruoyi.web.controller.system;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

import com.ruoyi.common.core.domain.entity.ItoResume;
import com.ruoyi.system.mapper.ItoResumeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.*;
import java.util.List;

@Controller
@RequestMapping("/exportPdf")
public class ExportPDFController extends BaseController {

    @Autowired
    ItoResumeMapper itoResumeMapper;

    @PostMapping("/getTestPdf")
    public void text(ItoResume itoResume){
        System.out.println(itoResume);
    }

    @PostMapping("/getPdf")
    public void excelPdf(HttpServletRequest request, HttpServletResponse response, ItoResume itoResume) throws Exception {
        List<ItoResume> resumes = itoResumeMapper.selectItoResumeList(itoResume);
        for (ItoResume resume : resumes) {
        //设置响应格式等
        response.setContentType("application/pdf");
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        Map<String, Object> map = new HashMap<>();
        //设置纸张规格为A4纸
        Rectangle rect = new Rectangle(PageSize.A4);
        //创建文档实例
        Document doc = new Document(rect);
        //添加中文字体
        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        //设置字体样式
        Font textFont = new Font(bfChinese, 11, Font.NORMAL); //正常
        //Font redTextFont = new Font(bfChinese,11,Font.NORMAL,Color.RED); //正常,红色
        Font boldFont = new Font(bfChinese, 11, Font.BOLD); //加粗
        //Font redBoldFont = new Font(bfChinese,11,Font.BOLD,Color.RED); //加粗,红色
        Font firsetTitleFont = new Font(bfChinese, 22, Font.BOLD); //一级标题
        Font secondTitleFont = new Font(bfChinese, 15, Font.BOLD, CMYKColor.BLUE); //二级标题
        Font underlineFont = new Font(bfChinese, 11, Font.UNDERLINE); //下划线斜体
        //设置字体
        com.itextpdf.text.Font FontChinese24 = new com.itextpdf.text.Font(bfChinese, 24, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font FontChinese18 = new com.itextpdf.text.Font(bfChinese, 18, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font FontChinese16 = new com.itextpdf.text.Font(bfChinese, 16, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font FontChinese12 = new com.itextpdf.text.Font(bfChinese, 12, com.itextpdf.text.Font.NORMAL);
        com.itextpdf.text.Font FontChinese11Bold = new com.itextpdf.text.Font(bfChinese, 11, com.itextpdf.text.Font.BOLD);
        com.itextpdf.text.Font FontChinese11 = new com.itextpdf.text.Font(bfChinese, 11, com.itextpdf.text.Font.ITALIC);
        com.itextpdf.text.Font FontChinese11Normal = new com.itextpdf.text.Font(bfChinese, 11, com.itextpdf.text.Font.NORMAL);

        //设置要导出的pdf的标题
        String title = resume.getName();
        response.setHeader("Content-disposition", "attachment; filename=".concat(String.valueOf(URLEncoder.encode(title + ".pdf", "UTF-8"))));
        OutputStream out = response.getOutputStream();
        PdfWriter.getInstance(doc, out);
        doc.open();
        doc.newPage();
        //新建段落
        //使用二级标题 颜色为蓝色
        Paragraph p1 = new Paragraph("个人简历", secondTitleFont);
        //设置行高
        p1.setLeading(0);
        //设置标题居中
        p1.setAlignment(Element.ALIGN_CENTER);
        //将段落添加到文档上
        doc.add(p1);
        //设置一个空的段落，行高为18  什么内容都不显示
        Paragraph blankRow1 = new Paragraph(18f, " ", FontChinese11);
        doc.add(blankRow1);
        //新建表格 列数为2
        PdfPTable table1 = new PdfPTable(2);
        //给表格设置宽度
        int width1[] = {10000, 10000};
        table1.setWidths(width1);
        //新建单元格

//            Long id = resume.getId();
//
//            int i = Integer.parseInt(String.valueOf(id));
            String name = resume.getName();
            Date birthday = resume.getBirthday();
            String sex = resume.getSex();
            String phone = resume.getPhone();
            Date workingTime = resume.getWorkingTime();
            String currentJobStatus = resume.getCurrentJobStatus();
            String workplacePeople = resume.getWorkplacePeople();
            String personalAdvantages = resume.getPersonalAdvantages();
            String jobType = resume.getJobType();
            String expectedPosition = resume.getExpectedPosition();
            String salaryRequirements = resume.getSalaryRequirements();
            String workCity = resume.getWorkCity();
            String expectedIndustry = resume.getExpectedIndustry();
            String corporateName = resume.getCorporateName();
            String department = resume.getDepartment();
            Date serviceTime = resume.getServiceTime();
            String industry = resume.getIndustry();
            String positionName = resume.getPositionName();
            String jobContent = resume.getJobContent();
            String haveSkills = resume.getHaveSkills();
            String entryName = resume.getEntryName();
            String projectRole = resume.getProjectRole();
            Date startTime = resume.getStartTime();
            Date endTime = resume.getEndTime();
            String projectDescription = resume.getProjectDescription();
            String schoolName = resume.getSchoolName();
            String typeSchooling = resume.getTypeSchooling();
            String education = resume.getEducation();
            String major = resume.getMajor();

            //给单元格赋值 每个单元格为一个段落，每个段落的字体为加粗
            PdfPCell cell11 = new PdfPCell(new Paragraph("姓名：  " + name, boldFont));
            PdfPCell cell12 = new PdfPCell(new Paragraph("生日：  " + birthday, boldFont));
            PdfPCell cell13 = new PdfPCell(new Paragraph("性别：  " + sex, boldFont));
            PdfPCell cell14 = new PdfPCell(new Paragraph("电话：  " + phone, boldFont));
            PdfPCell cell15 = new PdfPCell(new Paragraph("参加工作时间：  " + workingTime, boldFont));
            PdfPCell cell16 = new PdfPCell(new Paragraph("当前求职状态：  " + currentJobStatus, boldFont));
            PdfPCell cell17 = new PdfPCell(new Paragraph("职场人：  " + workplacePeople, boldFont));
            PdfPCell cell18 = new PdfPCell(new Paragraph("个人优势：  " + personalAdvantages, boldFont));
            PdfPCell cell19 = new PdfPCell(new Paragraph("求职类型：  " + expectedPosition, boldFont));
            PdfPCell cell110 = new PdfPCell(new Paragraph("期望职位：  " + salaryRequirements, boldFont));
            PdfPCell cell111 = new PdfPCell(new Paragraph("薪资要求：  " + workCity, boldFont));
            PdfPCell cell112 = new PdfPCell(new Paragraph("工作城市：  " + expectedIndustry, boldFont));
            PdfPCell cell113 = new PdfPCell(new Paragraph("期望行业：  " + corporateName, boldFont));
            PdfPCell cell114 = new PdfPCell(new Paragraph("公司名称：  " + department, boldFont));
            PdfPCell cell115 = new PdfPCell(new Paragraph("所属部门：  " + serviceTime, boldFont));
            PdfPCell cell116 = new PdfPCell(new Paragraph("在职时间：  " + industry, boldFont));
            PdfPCell cell117 = new PdfPCell(new Paragraph("所属行业：  " + positionName, boldFont));
            PdfPCell cell118 = new PdfPCell(new Paragraph("职位名称：  " + jobContent, boldFont));
            PdfPCell cell119 = new PdfPCell(new Paragraph("工作内容：  " + haveSkills, boldFont));
            PdfPCell cell120 = new PdfPCell(new Paragraph("拥有技能：  " + entryName, boldFont));
            PdfPCell cell121 = new PdfPCell(new Paragraph("项目名称：  " + projectRole, boldFont));
            PdfPCell cell122 = new PdfPCell(new Paragraph("项目角色：  " + startTime, boldFont));
            PdfPCell cell123 = new PdfPCell(new Paragraph("开始时间：  " + endTime, boldFont));
            PdfPCell cell124 = new PdfPCell(new Paragraph("结束时间：  " + projectDescription, boldFont));
            PdfPCell cell125 = new PdfPCell(new Paragraph("项目描述：  " + schoolName, boldFont));
            PdfPCell cell126 = new PdfPCell(new Paragraph("学校名称：  " + schoolName, boldFont));
            PdfPCell cell127 = new PdfPCell(new Paragraph("学制类型：  " + typeSchooling, boldFont));
            PdfPCell cell128 = new PdfPCell(new Paragraph("学历：  " + education, boldFont));
            PdfPCell cell129 = new PdfPCell(new Paragraph("学校名称：  " + major, boldFont));
            PdfPCell cell130 = new PdfPCell(new Paragraph("专业：  " + sex, boldFont));

            //设置单元格边框为0
            cell11.setBorder(0);
            cell12.setBorder(0);
            cell13.setBorder(1);
            cell14.setBorder(1);
            cell15.setBorder(0);
            cell16.setBorder(0);
            cell17.setBorder(1);
            cell18.setBorder(1);
            cell19.setBorder(0);
            cell110.setBorder(0);
            cell111.setBorder(1);
            cell112.setBorder(1);
            cell113.setBorder(0);
            cell114.setBorder(0);
            cell115.setBorder(0);
            cell116.setBorder(0);
            cell117.setBorder(0);
            cell118.setBorder(0);
            cell119.setBorder(0);
            cell120.setBorder(0);
            cell121.setBorder(0);
            cell122.setBorder(0);
            cell123.setBorder(0);
            cell124.setBorder(0);
            cell125.setBorder(0);
            cell126.setBorder(0);
            cell127.setBorder(0);
            cell128.setBorder(0);
            cell129.setBorder(0);
            cell130.setBorder(0);
            table1.addCell(cell11);
            table1.addCell(cell12);
            table1.addCell(cell13);
            table1.addCell(cell14);
            table1.addCell(cell15);
            table1.addCell(cell16);
            table1.addCell(cell17);
            table1.addCell(cell18);
            table1.addCell(cell19);
            table1.addCell(cell110);
            table1.addCell(cell111);
            table1.addCell(cell112);
            table1.addCell(cell113);
            table1.addCell(cell114);
            table1.addCell(cell115);
            table1.addCell(cell116);
            table1.addCell(cell117);
            table1.addCell(cell118);
            table1.addCell(cell119);
            table1.addCell(cell120);
            table1.addCell(cell121);
            table1.addCell(cell122);
            table1.addCell(cell123);
            table1.addCell(cell124);
            table1.addCell(cell125);
            table1.addCell(cell126);
            table1.addCell(cell127);
            table1.addCell(cell128);
            table1.addCell(cell129);
            table1.addCell(cell130);
            doc.add(table1);
            PdfPTable table3 = new PdfPTable(2);
            table3.setWidths(width1);
//        PdfPCell cell131 = new PdfPCell(new Paragraph("博客主页： https://blog.csdn.net/BADAO_LIUMANG_QIZHI  ",boldFont));
//        PdfPCell cell132 = new PdfPCell(new Paragraph("当前时间：  "+new Date().toString(),boldFont));
//        cell131.setBorder(0);
//        cell132.setBorder(0);
//        table3.addCell(cell131);
//        table3.addCell(cell132);
            doc.add(table3);
            doc.close();
        }
    }
}
 
