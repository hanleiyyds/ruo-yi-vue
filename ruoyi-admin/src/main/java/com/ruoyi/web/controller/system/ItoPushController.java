package com.ruoyi.web.controller.system;


import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.ItoPush;
import com.ruoyi.common.core.domain.entity.ItoPushVo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.service.IItoPushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2022-10-12
 */
@RestController
@RequestMapping("/system/push")
public class ItoPushController extends BaseController {
    @Autowired
    private IItoPushService itoPushService;

    /**
     * 查询列表
     */
    @PreAuthorize("@ss.hasPermi('system:push:list')")
    @GetMapping("/list")
    public TableDataInfo list(ItoPushVo itoPush) {
        itoPush.setSide(0);
        startPage();
        List<ItoPushVo> list = itoPushService.selectItoPushList(itoPush);
        return getDataTable(list);
    }

    /**
     * 查询列表
     */
    @PreAuthorize("@ss.hasPermi('system:push:list1')")
    @GetMapping("/list1")
    public TableDataInfo list1(ItoPushVo itoPush) {
        itoPush.setSide(1);
        startPage();
        List<ItoPushVo> list = itoPushService.selectItoPushList(itoPush);
        return getDataTable(list);
    }
    /**
     * 查询列表
     */
    @PreAuthorize("@ss.hasPermi('system:push:list1')")
    @GetMapping("/list2")
    public TableDataInfo list2(ItoPushVo itoPush) {
        itoPush.setSide(2);
        startPage();
        List<ItoPushVo> list = itoPushService.selectItoPushList(itoPush);
        return getDataTable(list);
    }

    //查询 TAHF测试专用
    @PreAuthorize("@ss.hasPermi('system:push:list')")
    @GetMapping("/list/{id}")
    public AjaxResult listNull(@PathVariable("id") Long id)
    {
        return AjaxResult.success(itoPushService.selectItoPushById(id));
    }


    /**
     * 导出列表
     */
    @PreAuthorize("@ss.hasPermi('system:push:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ItoPushVo itoPush)
    {
        List<ItoPushVo> list = itoPushService.selectItoPushList(itoPush);
        ExcelUtil<ItoPushVo> util = new ExcelUtil<ItoPushVo>(ItoPushVo.class);
        util.exportExcel(response, list, "数据");
    }

    /**
     * 获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:push:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(itoPushService.selectItoPushById(id));
    }

    /**
     * 新增
     */
    @PreAuthorize("@ss.hasPermi('system:push:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ItoPush itoPush) {
        return toAjax(itoPushService.insertItoPush(itoPush));
    }




    /**
     * 修改
     */
    @PreAuthorize("@ss.hasPermi('system:push:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ItoPush itoPush) {
        return toAjax(itoPushService.updateItoPush(itoPush));
    }

    /**
     * 删除
     */
    @PreAuthorize("@ss.hasPermi('system:push:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(itoPushService.deleteItoPushByIds(ids));
    }

    /**
     * 面试通过状态
     */
    @PreAuthorize("@ss.hasPermi('system:push:updateSide')")
    @Log(title = "面试通过", businessType = BusinessType.DELETE)
    @DeleteMapping("updateSide/{ids}")
    public AjaxResult updateSide(@PathVariable Long[] ids) {
        return toAjax(itoPushService.updateItoPushByIds(ids));
    }

    /**
     * 面试通过状态
     */
    @PreAuthorize("@ss.hasPermi('system:push:updateSide1')")
    @Log(title = "面试不通过", businessType = BusinessType.DELETE)
    @DeleteMapping("updateSide1/{ids}")
    public AjaxResult updateSide1(@PathVariable Long[] ids) {
        return toAjax(itoPushService.updateItoPushByIds1(ids));
    }
}
